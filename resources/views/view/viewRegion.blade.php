@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
 
        <title>Liste des cabinets</title>
    </head>
    
    <body>
        <br>
        <br>
        <div class="container col-md-10">
            <h2 class="text-center">Liste des régions</h2>
            <br>
            <div class="container p-3 my-3 px-5">
                <div class="card-deck">
                    @foreach ($regions as $region)
                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{ route('showDepartement', [$region->region]) }}">{{$region->region}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <br>
        </div>
    </body>
</html>
@endsection