@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/medicament.css" />
 
        <title>Liste des médicaments</title>
    </head>
    
    <body>
        <div class="container p-3 my-3 px-5">
        
            <div class="card-deck">
                @foreach ($medicaments as $medicament)
                <div class="card mb-5" style="width:300px;flex: inherit;">
                    <img class="card-img-top" src="http://loremflickr.com/400/300/pills" alt="Card image" style="width:100%">
                    <div class="card-body " style="text-align: center">
                        <a href="{{ route('showMedicament', [$medicament->nom]) }}">{{$medicament->nom}}</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </body>
</html>
@endsection