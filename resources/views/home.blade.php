@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>

            <div class="container p-4 my-4 px-5">
                <div class="card-deck">
                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{route('showRegion')}}">Acceder a la liste des regions</a>
                        </div>
                    </div>

                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{route('showMedicament')}}">Acceder a la liste des médicaments</a>
                        </div>
                    </div>

                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{route('showUser')}}">Acceder a la liste des utilisateurs</a>
                        </div>
                    </div>

                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{route('showVisite')}}">Acceder a la liste des visites</a>
                        </div>
                    </div>

                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{route('showMetier')}}">Acceder a la liste des métiers</a>
                        </div>
                    </div>

                    <div class="card mb-5" style="width:300px;flex: inherit;">
                        <div class="card-body " style="text-align: center">
                            <a href="{{route('showProfil')}}">test profil</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection