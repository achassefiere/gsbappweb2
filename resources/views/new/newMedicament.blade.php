@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un médicament</title>
    </head>
    <body>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="{{route('addMedicament')}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire d'ajout d'un médicament</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Id">Id</label>
                        <input class="form-control" name="id" id="Id" value="Un id va vous être attribué" readonly="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="Nom">Nom</label>
                        <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du médicament">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Prix">Prix</label>
                        <input type="number" class="form-control" name="prix" id="Prix" placeholder="Prix du médicament">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection