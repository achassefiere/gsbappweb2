@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un département</title>
    </head>
    <body>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="{{route('addCabinet')}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire d'ajout d'un département</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="Id">Id</label>
                        <input class="form-control" name="id" id="Id" value="Un id va vous être attribué" readonly="">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="Code">Code</label>
                        <input type="number" class="form-control" name="code" id="Code" placeholder="Code du département">
                    </div>
                    <div class="form-group col-md">
                        <label for="Nom">Nom</label>
                        <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du département">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Region">Région</label>
                        <input type="text" class="form-control" name="region" id="Region" placeholder="Nom de la région">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection