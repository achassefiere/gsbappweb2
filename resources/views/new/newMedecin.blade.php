@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un médecin</title>
    </head>
    <body>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="{{route('addMedecin')}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire d'ajout d'un médecin</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="Id">Id</label>
                        <input class="form-control" name="id" id="Id" value="Un id va vous être attribué" readonly="">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="Nom">Nom</label>
                        <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du médecin">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Prenom">Prénom</label>
                        <input type="text" class="form-control" name="prenom" id="Prenom" placeholder="Prénom du médecin">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="Mail">Adresse Mail</label>
                        <input type="text" class="form-control" name="mail" id="Mail" placeholder="Mail du médecin">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Tel">Numéro de télephone</label>
                        <input type="number" class="form-control" name="tel" id="Tel" placeholder="Télephone du médecin">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Metier">Profession</label>
                        <input type="text" class="form-control" name="metier" id="Metier" placeholder="Profession du médecin">
                    </div>
                    <div class="form-group col-md">
                        <label for="Cabinet">Cabinet Médical</label>
                        <input type="text" class="form-control" name="cabinet" id="Cabinet" placeholder="Cabinet du médecin">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection