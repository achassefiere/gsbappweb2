@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un cabinet</title>
    </head>
    <body>
        <br>
        <br>

        
        <form id="login-form" class="form" method="POST" action="{{route('addCabinet')}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire d'ajout d'un cabinet médical</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Id">Id</label>
                        <input class="form-control" name="id" id="Id" value="Un id va vous être attribué automatiquement" readonly="">
                    </div>
                    
                    <div class="form-group col-md">
                        <label for="nom">Nom</label>
                        <input class="form-control" name="nom" id="nom" placeholder="Nom du cabinet médical">
                    </div>
                </div>
                
                
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="codepostal">Code Postal</label>
                        <input type="number" class="form-control" name="codepostal" id="codepostal" placeholder="Code Postal">
                    </div>
                    <div class="form-group col-md">
                        <label for="adresse">Adresse</label>
                        <input type="text" class="form-control" name="adresse" id="adresse" placeholder="Adresse du cabinet">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Ville">Ville</label>
                        <input type="text" class="form-control" name="ville" id="Ville" placeholder="Ville du cabinet">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Departement">Département</label>
                        <select type="number" name="departement" id="Departement" class="form-control">
                            <option selected>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Ajouter</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection