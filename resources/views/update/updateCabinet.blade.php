@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un cabinet</title>
    </head>
    <body>
        <br>
        <br>

        
        <form id="login-form" class="form" method="post" action="{{route('updateCabinet', ['id' => $cabinets->id])}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire de modification d'un cabinet médical</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Id">Id</label>
                        <input class="form-control" id="id" value="Un id va vous être attribué automatiquement" readonly="">
                    </div>
                    
                    <div class="form-group col-md">
                        <label for="nom">Nom</label>
                        <input type="text" class="form-control" name="nom" id="nom" value="{{$cabinets->nom}}">
                    </div>
                </div>
                
                
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="codepostal">Code Postal</label>
                        <input type="number" class="form-control" name="codepostal" id="codepostal" placeholder="Code Postal" value="{{$cabinets->codepostal}}">
                    </div>
                    <div class="form-group col-md">
                        <label for="adresse">Adresse</label>
                        <input type="text" class="form-control" name="adresse" id="adresse" placeholder="Adresse du cabinet" value="{{$cabinets->adresse}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="ville">Ville</label>
                        <input type="text" class="form-control" name="ville" id="ville" placeholder="Ville du cabinet" value="{{$cabinets->ville}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="departement">Département</label>
                        <select type="number" name="departement" id="departement" class="form-control">
                            <option selected>{{$cabinets->departement_id}}</option>
                            <option>69</option>
                            <option>42</option>
                            <option>38</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection