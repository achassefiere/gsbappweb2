@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un médicament</title>
    </head>
    <body>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="{{route('updateMedicament', ['id' => $medicaments->id])}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire de modification d'un médicament</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Id">Id</label>
                        <input class="form-control" id="Id" value="Un id va vous être attribué" readonly="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="Nom">Nom</label>
                        <input type="text" class="form-control" id="Nom" placeholder="Nom du médicament" value="{{$medicaments->nom}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Prix">Prix</label>
                        <input type="number" class="form-control" id="Prix" placeholder="Prix du médicament" value="{{$medicaments->prix}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection