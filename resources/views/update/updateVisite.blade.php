@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'une visite</title>
    </head>
    <body>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="{{route('updateVisite', ['id' => $visites->id])}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire de modification d'une visite</h3>
            <div class="container">
                <div class="form-group col-md-8">
                        <label for="Intitule">Intitulé de la visite</label>
                        <input type="text" class="form-control" name="intitule" id="Intitule" placeholder="Intitulé de la visite" value="{{$visites->intitule}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="Id">Id</label>
                        <input class="form-control" id="Id" value="Un id va vous être attribué" readonly="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="Date">Date</label>
                        <input type="date" class="form-control" id="Date" placeholder="Date de la visite" value="{{$visites->date}}">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="Heure">Heure</label>
                            <select type="number" id="Heure" class="form-control">
                                <option selected>08h00</option>
                                <option>08h30</option>
                                <option>09h00</option>
                                <option>09h30</option>
                                <option>10h00</option>
                                <option>10h30</option>
                                <option>11h00</option>
                                <option>11h30</option>
                                <option>13h00</option>
                                <option>13h30</option>
                                <option>14h00</option>
                                <option>14h30</option>
                                <option>15h00</option>
                                <option>15h30</option>
                                <option>16h00</option>
                                <option>16h30</option>
                                <option>17h00</option>
                                <option>17h30</option>
                                <option>18h00</option>
                                <option>18h30</option>
                            </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="Medecin">Nom du médecin</label>
                        <input type="text" class="form-control" name="medecin" id="Medecin" placeholder="Nom du médecin visité" value="{{$visites->medecin_id}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Medicament">Médicament</label>
                        <input type="text" class="form-control" name="medicament" id="Medicament" placeholder="Nom du médicament présenté" value="{{$visites->medicament_id}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection