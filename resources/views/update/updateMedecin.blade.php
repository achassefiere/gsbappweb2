@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ajout d'un médecin</title>
    </head>
    <body>
        <br>
        <br>
        <form id="login-form" class="form" method="POST" action="{{route('updateMedecin', ['id' => $medecins->id])}}">
        @csrf
            <h3 class="text-center text" name="txt">Formulaire de modification d'un médecin</h3>
            <div class="container">
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="Id">Id</label>
                        <input class="form-control" name="id" id="Id" value="Un id va vous être attribué" readonly="">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="Nom">Nom</label>
                        <input type="text" class="form-control" name="nom" id="Nom" placeholder="Nom du médecin" value="{{$medecins->nom}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Prenom">Prénom</label>
                        <input type="text" class="form-control" name="prenom" id="Prenom" placeholder="Prénom du médecin" value="{{$medecins->prenom}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="Mail">Adresse Mail</label>
                        <input type="text" class="form-control" name="mail" id="Mail" placeholder="Mail du médecin" value="{{$medecins->mail}}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Tel">Numéro de télephone</label>
                        <input type="number" class="form-control" name="tel" id="Tel" placeholder="Télephone du médecin" value="{{$medecins->tel}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="Metier">Profession</label>
                        <input type="text" class="form-control" name="metier" id="Metier" placeholder="Profession du médecin" value="{{$medecins->metier_id}}">
                    </div>
                    <div class="form-group col-md">
                        <label for="Cabinet">Cabinet Médical</label>
                        <input type="text" class="form-control" name="cabinet" id="Cabinet" placeholder="Cabinet du médecin" value="{{$medecins->cabinet_id}}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-6">
                        <button type="submit" class="btn btn-success" name="btnadd">Modifier</button>
                        
                        <!--le bouton reset permet de revenir a l'etat initial du formulaire -->
                        
                        <button type="reset" class="btn btn-warning" name="btnreset">Recharger</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
@endsection