<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Departement;


class DepartementController extends Controller
{
    public function show_region() {

        $regions = Departement::all()->unique('region');
        return view("view.viewRegion", ["regions" => $regions]);
    }
    
    public function show_departement($region) {

        $departements = Departement::all()->where('region','=',$region);
        return view("view.viewDepartement", ["departements" => $departements,
                                        "region" => $region]);
    }

    public function new_departement() {

        return view("new.newDepartement");
    }

    public function add_departement(Request $request) {

        $departement = new Departement;
        $departement->nom = $request->nom;
        $departement->code = $request->code;
        $departement->region = $request->region;
        $departement->save();
        
        return redirect('/home/departement');
    }

    public function edit_departement($id) {

        $departements = Departement::find($id);
        return view('update.updateDepartement',['departements'=>$departements]);

    }

    public function update_departement(Request $request,$id) {

        $nom = $request->input('nom');
        $code = $request->input('code');
        $region = $request->input('region');
        
        DB::update('update departement set nom = ?, code = ?, region = ?  where id = ?',
        [$nom, $code, $region, $id]);

        return redirect('/home/departement');
    }

    public function delete_departement($id) {

        DB::delete('delete from departement where id = ?',[$id]);
        return redirect('/home/departement');
    }
    
}