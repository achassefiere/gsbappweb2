<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Metier;


class MetierController extends Controller
{
    public function show_metier() {

        $metiers = Metier::all();
        return view("view.viewMetier", ["metiers" => $metiers]);
    }

    public function new_metier() {

        return view("new.newMetier");
    }

    public function add_metier(Request $request) {

        $metier = new Metier;
        $metier->nom = $request->nom;
        $metier->tarifConsultation = $request->tarif;
        $metier->save();
        
        return redirect('/home/metier');
    }

    public function edit_metier($id) {

        $metiers = Metier::find($id);
        return view('update.updateMetier',['metiers'=>$metiers]);

    }

    public function update_metier(Request $request,$id) {

        $nom = $request->input('nom');
        $tarif = $request->input('tarif');
        
        DB::update('update metier set nom = ?, tarifConsultation = ? where id = ?',
        [$nom, $tarif, $id]);

        return redirect('/home/metier');
    }

    public function delete_metier($id) {

        DB::delete('delete from metier where id = ?',[$id]);
        return redirect('/home/metier');
    }
    
}