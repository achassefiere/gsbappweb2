<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Medecin;


class MedecinController extends Controller
{
    public function show_medecin($cabinet) {

        $medecins = Medecin::all()->where('cabinet_id','=',$cabinet);
        return view("view.viewMedecin", ["medecins" => $medecins,
                                    "cabinet" => $cabinet]);
    }

    public function new_medecin() {

        return view("new.newMedecin");
    }

    public function show_profil() {

        return view("view.viewProfilMedecin");
    }

    public function add_medecin(Request $request) {

        $medecin = new Medecin;
        $medecin->nom = $request->nom;
        $medecin->prenom = $request->prenom;
        $medecin->tel = $request->tel;
        $medecin->mail = $request->mail;
        $medecin->cabinet_id = $request->cabinet;
        $medecin->metier_id = $request->metier;
        $medecin->save();
        
        return redirect('/home/medecin');
    }

    public function edit_medecin($id) {

        $medecins = Medecin::find($id);
        return view('update.updateMedecin',['medecins'=>$medecins]);

    }

    public function update_medecin(Request $request,$id) {

        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $tel = $request->input('tel');
        $mail = $request->input('mail');
        $cabinet = $request->input('cabinet');
        $metier = $request->input('metier');
        
        DB::update('update medecin set nom = ?, prenom = ?, tel = ?, mail = ?, cabinet_id = ?, metier_id = ? where id = ?',
        [$nom, $prenom, $tel, $mail, $cabinet, $metier, $id]);

        return redirect('/home/medecin');
    }

    public function delete_medecin($id) {

        DB::delete('delete from medecin where id = ?',[$id]);
        return redirect('/home/medecin');
    }
    
}