<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;


class UserController extends Controller
{
    public function show_user() {

        $users = User::all();
        return view("view.viewUser", ["users" => $users]);
    }

    public function new_user() {

        return view("new.newUser");
    }

    public function add_user(Request $request) {

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
        
        return redirect('/home/user');
    }

    public function edit_user($id) {

        $users = User::find($id);
        return view('update.updateUser',['users'=>$users]);

    }

    public function update_user(Request $request,$id) {

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        
        DB::update('update users set name = ?, email = ?, password = ? where id = ?',
        [$name, $email, $password, $id]);

        return redirect('/home/user');
    }

    public function delete_user($id) {

        DB::delete('delete from user where id = ?',[$id]);
        return redirect('/home/user');
    }
    
}