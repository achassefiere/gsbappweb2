<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Visite;


class VisiteController extends Controller
{
    public function show_visite() {

        $visites = Visite::all();
        return view("view.viewVisite", ["visites" => $visites]);
    }

    public function new_visite() {

        return view("new.newVisite");
    }

    public function add_visite(Request $request) {

        $visite = new Visite;
        $visite->intitule = $request->intitule;
        $visite->date = $request->date;
        $visite->horaire = $request->horaire;
        $visite->medecin_id = $request->medecin;
        $visite->medicament_id = $request->medicament;
        $visite->save();
        
        return redirect('/home/visite');
    }

    public function edit_visite($id) {

        $visites = Visite::find($id);
        return view('update.updateVisite',['visites'=>$visites]);

    }

    public function update_visite(Request $request,$id) {

        $intitule = $request->input('intitule');
        $code = $request->input('code');
        $region = $request->input('adresse');
        
        DB::update('update visite set nom = ?, code = ?, adresse = ?, region = ?',
        [$nom, $code, $region, $id]);

        return redirect('/home/visite');
    }

    public function delete_visite($id) {

        DB::delete('delete from visite where id = ?',[$id]);
        return redirect('/home/visite');
    }
    
}