<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cabinet;


class CabinetController extends Controller
{

    public function show_cabinet($departement) {

        $cabinets = Cabinet::all()->where('departement_id','=',$departement);
        return view("view.viewCabinet", ["cabinets" => $cabinets,
                                    "departement" => $departement]);
    }

    public function new_cabinet() {

        return view("new.newCabinet");
    }

    public function add_cabinet(Request $request) {

        $cabinet = new Cabinet;
        $cabinet->nom = $request->nom;
        $cabinet->codepostal = $request->codepostal;
        $cabinet->adresse = $request->adresse;
        $cabinet->ville = $request->ville;
        $cabinet->departement_id = $request->departement;
        $cabinet->save();
        
        return redirect('/home/cabinet');
    }

    public function edit_cabinet($id) {

        $cabinets = Cabinet::find($id);
        return view('update.updateCabinet',['cabinets'=>$cabinets]);

    }

    public function update_cabinet(Request $request,$id) {

        $nom = $request->input('nom');
        $codepostal = $request->input('codepostal');
        $adresse = $request->input('adresse');
        $ville = $request->input('ville');
        $departement = $request->input('departement');
        
        DB::update('update cabinet set nom = ?, codepostal = ?, adresse = ?, ville = ?, departement_id = ? where id = ?',
        [$nom, $codepostal, $adresse, $ville, $departement, $id]);

        return redirect('/home/cabinet');
    }

    public function delete_cabinet($id) {

        DB::delete('delete from cabinet where id = ?',[$id]);
        return redirect('/home/cabinet');
    }

    /*public function paginate()
    {
        return view('cabinet.index', [
            'cabinet' => DB::table('cabinet')->paginate(15)
        ]);
    }

    public function edit_cabinet($id) {
        $cabinets = Cabinet::find($id);
        //$cabinets = DB::table('cabinet')->select()->where('id','=',$id)->first();
        //ddd($cabinets);
        //ddd($cabinets->departement);
        //$cabinets = DB::select('select * from cabinet where id = ?',[$id]);
        //$cabinets = Cabinet::find($id);
        //ddd($cabinets);
        return view('updateCabinet',['cabinets'=>$cabinets]);

    }*/
    
}