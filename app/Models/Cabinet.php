<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cabinet extends Model
{
    use HasFactory;
    protected $table = 'cabinet';
    public $incrementing = true;

    protected $fillable = [
        'nom', 
        'codepostal',
        'adresse', 
        'ville',
        'departement_id' //'id'->departement
    ];
    
    public function departement() {
        return $this->hasOne(Departement::class, 'id', 'departement_id');
    }
}
