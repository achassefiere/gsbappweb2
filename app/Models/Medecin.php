<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medecin extends Model
{
    use HasFactory;
    protected $table = 'medecin';
    public $incrementing = true;

    public function cabinet() {
        return $this->hasOne(Cabinet::class, 'id', 'cabinet_id');
    }

    public function metier() {
        return $this->hasOne(Metier::class, 'id', 'metier_id');
    }
}
