<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         //\App\Models\Departement::factory(10)->create();
         \App\Models\Metier::factory(5)->create();
         \App\Models\Medicament::factory(10)->create();
         \App\Models\Roles::factory(5)->create();
         \App\Models\User::factory(10)->create();
         \App\Models\Cabinet::factory(100)->create();
         \App\Models\Medecin::factory(100)->create();
         \App\Models\Visite::factory(50)->create();
        // on implémente tous les seeder ici pour pouvoir hydrater toutes les tables d'un coup
        
    }
}