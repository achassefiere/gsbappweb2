<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCabinetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabinet', function (Blueprint $table) {
            $table->id();
            $table->char('nom', 50);
            $table->integer('codepostal')->length(5);
            $table->char('adresse', 50);
            $table->string('ville', 50);
            $table->unsignedBigInteger('departement_id');
            $table->foreign('departement_id')->references('id')->on('departement')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabinet');
    }
}
