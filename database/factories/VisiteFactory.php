<?php

namespace Database\Factories;

use App\Models\Visite;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class VisiteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Visite::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'intitule' => $this->faker->sentence(6, true),
            'date' => $this->faker->date('2000-01-01', 'now'),
            'horaire' => $this->faker->time('08:00:00', '19:00:00'),
            'medecin_id' => DB::table('medecin')->select('id')->inRandomOrder()->first()->id,
            'user_id' => DB::table('user')->select('id')->inRandomOrder()->first()->id,
            'medicament_id' => DB::table('medicament')->select('id')->inRandomOrder()->first()->id,
        ];
    }
}
