<?php

namespace Database\Factories;

use App\Models\Cabinet;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class CabinetFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cabinet::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->unique()->company,
            'codepostal' => $this->faker->randomNumber(5),
            'adresse' => $this->faker->unique()->streetAddress,
            'ville' => $this->faker->unique()->city,
            'departement_id' => DB::table('departement')->select('id')->inRandomOrder()->first()->id,
        ];
    }
}
