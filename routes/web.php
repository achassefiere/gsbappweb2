<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\defaultController;
use App\Http\Controllers\CabinetController;
use App\Http\Controllers\MedecinController;
use App\Http\Controllers\MedicamentController;
use App\Http\Controllers\VisiteController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MetierController;
use App\Http\Controllers\DepartementController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { //la route au lancement de l'application
    return view('../auth/login');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
        ->name('home');//on utilise une route nommée comme ca on peut changer la route sans changer son nom
Auth::routes();



//ROUTES CABINET

Route::get('/home/cabinet/{departement_id}' , [CabinetController::class ,'show_cabinet'])
        ->name('showCabinet');
Route::get('/home/cabinet/new_cabinet' , [CabinetController::class ,'new_cabinet'])
        ->name('newCabinet');
Route::post('/home/cabinet/add_cabinet' , [CabinetController::class ,'add_cabinet'])
        ->name('addCabinet');
Route::get('/home/cabinet/edit_cabinet/{id}' , [CabinetController::class ,'edit_cabinet'])
        ->name('editCabinet');
Route::post('/home/cabinet/edit_cabinet/update_cabinet/{id}' , [CabinetController::class ,'update_cabinet'])
        ->name('updateCabinet');
Route::get('/home/cabinet/delete_cabinet/{id}' , [CabinetController::class ,'delete_cabinet'])
        ->name('deleteCabinet');


//ROUTES DEPARTEMENT
Route::get('/home/region' , [DepartementController::class ,'show_region'])
        ->name('showRegion');
Route::get('/home/departement/{region}' , [DepartementController::class ,'show_departement'])
        ->name('showDepartement');
Route::get('/home/departement/new_departement' , [DepartementController::class ,'new_departement'])
        ->name('newDepartement');
Route::post('/home/departement/{region}/add_departement' , [DepartementController::class ,'add_departement'])
        ->name('addDepartement');
Route::get('/home/departement/edit_departement/{id}' , [DepartementController::class ,'edit_departement'])
        ->name('editDepartement');
Route::post('/home/departement/edit_departement/update_departement{id}' , [DepartementController::class ,'update_departement'])
        ->name('updateDepartement');
Route::get('/home/departement/delete_departement/{id}' , [DepartementController::class ,'delete_departement'])
        ->name('deleteDepartement');


//ROUTES MEDECIN

Route::get('/home/medecin/{cabinet_id}' , [MedecinController::class ,'show_medecin'])
        ->name('showMedecin');
Route::get('/home/profil' , [MedecinController::class ,'show_profil'])
        ->name('showProfil');
Route::get('/home/medecin/new_medecin' , [MedecinController::class ,'new_medecin'])
        ->name('newMedecin');
Route::post('/home/medecin/add_medecin' , [MedecinController::class ,'add_medecin'])
        ->name('addMedecin');
Route::get('/home/medecin/edit_medecin/{id}' , [MedecinController::class ,'edit_medecin'])
        ->name('editMedecin');
Route::post('/home/medecin/update_medecin' , [MedecinController::class ,'update_medecin'])
        ->name('updateMedecin');
Route::get('/home/medecin/delete_medecin/{id}' , [MedecinController::class ,'delete_medecin'])
        ->name('deleteMedecin');


//ROUTES USER

Route::get('/home/user' , [UserController::class ,'show_user'])
        ->name('showUser');
Route::get('/home/user/new_user' , [UserController::class ,'new_user'])
        ->name('newUser');
Route::post('/home/user/add_user' , [UserController::class ,'add_user'])
        ->name('addUser');
Route::get('/home/user/edit_user/{id}' , [UserController::class ,'edit_user'])
        ->name('editUser');
Route::post('/home/user/update_user' , [UserController::class ,'update_user'])
        ->name('updateUser');
Route::get('/home/user/delete_user/{id}' , [UserController::class ,'delete_user'])
        ->name('deleteUser');


//ROUTES MEDICAMENT

Route::get('/home/medicament' , [MedicamentController::class ,'show_medicament'])
        ->name('showMedicament');
Route::get('/home/medicament/infos_medicament' , [MedicamentController::class ,'show_infos_medicament'])
        ->name('showInfosMedicament');
Route::get('/home/medicament/new_medicament' , [MedicamentController::class ,'new_medicament'])
        ->name('newMedicament');
Route::post('/home/medicament/add_medicament' , [MedicamentController::class ,'add_medicament'])
        ->name('addMedicament');
Route::get('/home/medicament/edit_medicament/{id}' , [MedicamentController::class ,'edit_medicament'])
        ->name('editMedicament');
Route::post('/home/medicament/update_medicament' , [MedicamentController::class ,'update_medicament'])
        ->name('updateMedicament');
Route::get('/home/medicament/delete_medicament/{id}' , [MedicamentController::class ,'delete_medicament'])
        ->name('deleteMedicament');

//ROUTES METIER

Route::get('/home/metier' , [MetierController::class ,'show_metier'])
        ->name('showMetier');
Route::get('/home/metier/new_metier' , [MetierController::class ,'new_metier'])
        ->name('newMetier');
Route::post('/home/metier/add_metier' , [MetierController::class ,'add_metier'])
        ->name('addMetier');
Route::get('/home/metier/edit_metier/{id}' , [MetierController::class ,'edit_metier'])
        ->name('editMetier');
Route::post('/home/metier/update_metier' , [MetierController::class ,'update_metier'])
        ->name('updateMetier');
Route::get('/home/metier/delete_metier/{id}' , [MetierController::class ,'delete_metier'])
        ->name('deleteMetier');

//ROUTES VISITE

Route::get('/home/visite' , [VisiteController::class ,'show_visite'])
        ->name('showVisite');
Route::get('/home/visite/new_visite' , [VisiteController::class ,'new_visite'])
        ->name('newVisite');
Route::post('/home/visite/add_visite' , [VisiteController::class ,'add_visite'])
        ->name('addVisite');
Route::get('/home/visite/edit_visite/{id}' , [VisiteController::class ,'edit_visite'])
        ->name('editVisite');
Route::post('/home/visite/update_visite' , [VisiteController::class ,'update_visite'])
        ->name('updateVisite');
Route::get('/home/visite/delete_visite/{id}' , [VisiteController::class ,'delete_visite'])
        ->name('deleteVisite');